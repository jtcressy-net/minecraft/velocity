# This file is a template, and might need editing before it works on your project.
FROM openjdk:8-alpine
MAINTAINER Joel Cressy joel@jtcressy.net

ENV VELOCITY_JAR_URL=https://ci.velocitypowered.com/job/velocity/192/artifact/proxy/build/libs/velocity-proxy-1.0.5-all.jar

ENV JAVA_MEMORY=512M
ENV JAVA_ARGS=

RUN mkdir -p /velocity/plugins

WORKDIR /velocity

RUN wget -O velocity.jar $VELOCITY_JAR_URL

CMD ["/bin/sh", "-c", "java -Xmx$JAVA_MEMORY -Xms$JAVA_MEMORY $JAVA_ARGS -jar velocity.jar"]

